# Change log

## v1.0.1 — June. 04, 2020

**Bug fixes:**
- Fix a typo

---

## v1.0.0 — June. 04, 2020

Initial release.

**Added:**
- Readme
- Change log
- License
